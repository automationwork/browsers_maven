package com.listners;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import org.testng.annotations.AfterSuite;


public class ExtentReport {
    public static ExtentReports extentReports;
    protected static ExtentTest extentTest;

    public static ExtentReports getExtentReport() {
        if (extentReports == null){
            extentReports = new ExtentReports(System.getProperty("user.dir") +"/ExtentReport/index.html", true);
        }
        return extentReports;
    }

    public static ExtentTest getExtentTest(String testName) {
        if (extentReports == null) {
            getExtentReport();
        }
        if (extentTest == null){
            extentTest = extentReports.startTest(testName);
            extentTest.log(LogStatus.INFO, testName + " configureation started");
        }
        return extentTest;
    }

    @AfterSuite
    public void endSuite(){
        ExtentReport.getExtentReport().close();
    }
}