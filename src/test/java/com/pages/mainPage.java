package com.pages;

import com.helpers.generateTestData;
import com.helpers.readWrite;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class mainPage {

    private int i = 0;

    public mainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    readWrite read = new readWrite();

    @FindBy(how = How.CLASS_NAME, using = "main-menu")
    List<WebElement> allMenuItems;

    @FindBy(how = How.CSS, using = ".meganav-links.list-unstyled.reveal>li>a")
    List<WebElement> allDestinations;


    @FindBy(how = How.CSS, using = ".meganav-links.list-unstyled.items>li>a")
    List<WebElement> allTravel;

    @FindBy(how = How.CSS, using = " .font-custom-clean.bg-text")
    List<WebElement> allAboutScotland;

    public void validateHeader() {
        int i = 0;
        int item_count = allMenuItems.size();
        if (item_count > 8) {
            System.out.println("More Items than expected");
        } else {
            while (i < item_count) {
                String text = allMenuItems.get(i).getText();
                if (i == 0) {
                    Assert.assertEquals("DESTINATIONS", text);
                } else if (i == 1) {
                    Assert.assertEquals("ACCOMMODATION", text);
                } else if (i == 2) {
                    Assert.assertEquals("SEE & DO", text);
                } else if (i == 3) {
                    Assert.assertEquals("HOLIDAYS", text);
                } else if (i == 4) {
                    Assert.assertEquals("TRAVEL", text);
                } else if (i == 5) {
                    Assert.assertEquals("ABOUT SCOTLAND", text);
                } else if (i == 6) {
                    Assert.assertEquals("EVENTS", text);
                }
                i = i + 1;
            }
        }
    }

    public void validateDestinations() {
        int x = 0;
        String mainpagedata = "C:\\Users\\hsidhu\\Documents\\AutomationProjects\\Demo\\browsers\\testdata\\mainpage.txt";
        for (x = 0; x <= 3; x++) {
            int j = 0;
            allMenuItems.get(x).click();
            int item_count = allDestinations.size();
            while (j < item_count) {
                String text = allDestinations.get(j).getText();
                for (String data : read.reading(mainpagedata)) {
                    if (data.contains(text)) {
//                      System.out.println("all good for " + data);
                        j= j + 1;
                        break;
                    }
                }
            }
        }
//        TRAVEL
        int y = 0;
        allMenuItems.get(4).click();
        int item_count = allTravel.size();
        while (y < item_count) {
            String text = allTravel.get(y).getText();
            for (String data : read.reading(mainpagedata)) {
                if (data.contains(text)) {
//                  System.out.println("all good for " + data);
                    y = y + 1;
                    break;
                }
            }
        }
//        ABOUT SCOTLAND
        int z = 0;
        allMenuItems.get(5).click();
        int count = allAboutScotland.size();
        while (z < count) {
            String text = allAboutScotland.get(z).getText();
            for (String data : read.reading(mainpagedata)) {
                if (data.contains(text)) {
                    System.out.println("all good for " + data);
                    z = z + 1;
                    break;
                }
            }
        }
//        allMenuItems.get(5).click();
//        int item_count = allDestinations.size();
//
//        for (int i = 0; i < allAboutScotland.size(); i++) {
//            String elementText = allAboutScotland.get(i).getText();
//            System.out.println(elementText);
//        }

    }

}
