package com.pages;

import com.webDriver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.concurrent.TimeUnit;

public class RetailUser {

    static WebDriver driver = Driver.getCurrentDriver();

    public static WebElement signin() {

        driver.get("http://automationpractice.com/index.php");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        return driver.findElement(By.cssSelector(".login"));
    }

    public static WebElement enteremail() {
        return driver.findElement(By.id("email_create"));
    }

    public void signinlink() {
        signin().click();
    }



}





//    public static WebElement registerNow() {
//        return driver.findElement(By.className("btn-border-grey"));
//    }
//
//    public static WebElement firstName() {
//        return driver.findElement(By.id("firstnameUKI"));
//    }
//
//    public static WebElement surname() {
//        return driver.findElement(By.id("surname"));
//    }
//
//    public static WebElement email() {
//        return driver.findElement(By.id("email"));
//    }
//
//    public static WebElement password() {
//        return driver.findElement(By.id("password"));
//    }
//
//    public static WebElement confirmpassword() {
//        return driver.findElement(By.id("confirm-password"));
//    }
//
//    public static WebElement selectSecurityQuestions() {
//        return driver.findElement(By.id("SQ-select-1"));
//    }
//
//}
//
//
////
////    @FindBy(how = How.ID, using = "questionList-0-answer")
////    public WebElement q1Answer;
////
////    @FindBy(how = How.CLASS_NAME, using = "tick")
////    List<WebElement> allTicks;
////
////    @FindBy(how = How.CSS, using = ".hidden-sm.hidden-xs.pull-left.desktop-vs-logo>a>img")
////    public WebElement homeicon;
////
////    @FindBy(how = How.CSS, using = "#top")
////    public WebElement goTop;
