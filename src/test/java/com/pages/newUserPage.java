package com.pages;

import com.base.BaseUtil;
import com.helpers.Helpers;
import com.helpers.generateTestData;
import com.helpers.readWrite;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("WeakerAccess")

public class newUserPage{
    int i = 0;

    String mainpath = "C:\\Users\\hsidhu\\Documents\\AutomationProjects\\Demo\\browsers\\testdata\\userlogindata.txt";

    public newUserPage(WebDriver driver) {PageFactory.initElements(driver, this);
    }

    generateTestData data = new generateTestData();
    readWrite write = new readWrite();
    readWrite read = new readWrite();
    Helpers waitme = new Helpers();

    @FindBy(how = How.CSS, using = ".btn-pink.closebtn")
    private WebElement acceptCookies;

//    @FindBy(how = How.ID, using = "create_account_error")
//    public WebElement message;

    @FindBy(how = How.XPATH, using = ".//*[@id='afHeaderLogin']/a/span")
    private WebElement selectLogin;

    @FindBy(how = How.CLASS_NAME, using = "btn-border-grey")
    private WebElement registerNow;

    @FindBy(how = How.ID, using = "firstnameUKI")
    private WebElement firstName;

    @FindBy(how = How.ID, using = "surnameUKI")
    public WebElement surname;

    @FindBy(how = How.ID, using = "email")
    private WebElement email;

    @FindBy(how = How.ID, using = "password")
    public WebElement password1;

    @FindBy(how = How.ID, using = "confirm-password")
    public WebElement password2;

    @FindBy(how = How.ID, using = "SQ-select-1")
    public WebElement selectSecurityQuestions;

    @FindBy(how = How.ID, using = "questionList-0-answer")
    public WebElement q1Answer;

    @FindBy(how = How.CLASS_NAME, using = "tick")
    List<WebElement> allTicks;

    @FindBy(how = How.CSS, using = ".hidden-sm.hidden-xs.pull-left.desktop-vs-logo>a>img")
    public WebElement homeicon;

    @FindBy(how = How.CSS, using = "#top")
    public WebElement goTop;

    /*CAPTCHA ISSUE */
//    @FindBy(how = How.CLASS_NAME, using = "recaptcha-checkbox-checkmark")
//    public WebElement robotcheck1;
//    @FindBy(how = How.CLASS_NAME, using = "recaptcha-checkbox-spinner")
//    public WebElement robotcheck2;

//    @FindBy(how = How.LINK_TEXT, using = "Sign Up")
//    public WebElement signUp;

    public void CreateNewAccount() {

        write.writing(data.getEmail(), mainpath);
        acceptCookies.click();
        selectLogin.click();
        registerNow.click();

//        if (message.getText().contains("email address has already been registered")) {
//            System.out.println("Account NOT Created \n");
//
//        }

        firstName.sendKeys(data.getFirstName());
        surname.sendKeys(data.getLastName());
        email.sendKeys(data.getEmail());
        password1.sendKeys("SopraSteria@12");
        password2.sendKeys("SopraSteria@12");

        selectSecurityQuestions.click();
        Select select = new Select(selectSecurityQuestions);
        select.selectByIndex(1);
        q1Answer.sendKeys("Spiderman");

        goTop.click();

    }
}
