package com.pages;

import com.webDriver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.util.List;

public class CreateUser {

    static WebDriver driver = Driver.getCurrentDriver();

    public static WebElement acceptCookies() {
        return driver.findElement(By.cssSelector(".btn-pink.closebtn"));
    }

    public static WebElement selectLogin() {
        return driver.findElement(By.cssSelector(".//*[@id='afHeaderLogin']/a/span"));
    }

    public static WebElement registerNow() {
        return driver.findElement(By.className("btn-border-grey"));
    }

    public static WebElement firstName() {
        return driver.findElement(By.id("firstnameUKI"));
    }

    public static WebElement surname() {
        return driver.findElement(By.id("surname"));
    }

    public static WebElement email() {
        return driver.findElement(By.id("email"));
    }

    public static WebElement password() {
        return driver.findElement(By.id("password"));
    }

    public static WebElement confirmpassword() {
        return driver.findElement(By.id("confirm-password"));
    }

    public static WebElement selectSecurityQuestions() {
        return driver.findElement(By.id("SQ-select-1"));
    }

}


//
//    @FindBy(how = How.ID, using = "questionList-0-answer")
//    public WebElement q1Answer;
//
//    @FindBy(how = How.CLASS_NAME, using = "tick")
//    List<WebElement> allTicks;
//
//    @FindBy(how = How.CSS, using = ".hidden-sm.hidden-xs.pull-left.desktop-vs-logo>a>img")
//    public WebElement homeicon;
//
//    @FindBy(how = How.CSS, using = "#top")
//    public WebElement goTop;
