package com.steps;

import com.beust.jcommander.Parameter;
import com.cucumberTest.TestRunner;
import com.helpers.Helpers;
import com.supportFactory.BrowserFactory;
import com.supportMethods.FileRead;
import com.webDriver.Driver;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import gherkin.lexer.He;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.*;
import java.util.Properties;

import static com.webDriver.Driver.takeScreenshot;

public class Hooks {
    private static Boolean runOnce = false;

////     @Before(order =1)
//    public void browsername() throws IOException {
//
//        File configFile = new File("src/test/resources/config.properties");
//
//        FileInputStream in = new FileInputStream(configFile);
//        Properties props = new Properties();
//        props.load(in);
//        in.close();
//
//        FileOutputStream out = new FileOutputStream(configFile);
//        props.setProperty("browser", "c");
//        props.store(out, null);
//        out.close();
//    }

    @Before(order = 1)
    public void beforeAll() throws IOException {

        if (!runOnce) {
            TestRunner.config = FileRead.readProperties();
        }

    }

    @Before(order = 2)
    public void before(Scenario scenario) {
        TestRunner.scenario = scenario;
    }


    @After
    public void after(Scenario scenario) {
        if (scenario.isFailed()) {
            //take screenshot
            try {
                Driver.takeScreenshot("src\\test\\resources\\snapshots\\fail.png");
                Driver.embedScreenshot();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
