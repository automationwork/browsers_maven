package com.steps;

import com.pages.RetailUser;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RetailShopping {
    @Given("^I am on the home page and select the sign in button$")
    public void iAmOnTheHomePageAndSelectTheSignInButton() {
        RetailUser.signin().click();

    }

    @When("^I provide a dummy email$")
    public void iProvideADummyEmail() {
        // Write code here that turns the phrase above into concrete actions
        RetailUser.enteremail().sendKeys("abc@abc.com");

    }

    @Then("^only one product is made available$")
    public void onlyOneProductIsMadeAvailable() {
        // Write code here that turns the phrase above into concrete actions
    }

    @And("^I Select Dresses and casual dresses$")
    public void iSelectDressesAndCasualDresses() {
        // Write code here that turns the phrase above into concrete actions
    }
}
