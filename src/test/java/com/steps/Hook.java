//package com.steps;
//
//import com.base.BaseUtil;
//import cucumber.api.Scenario;
//
//import cucumber.api.java.After;
//import cucumber.api.java.Before;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.opera.OperaDriver;
//import org.openqa.selenium.opera.OperaOptions;
//import org.testng.annotations.Parameters;
//
//import java.io.File;
//
//import static com.helpers.Helpers.takeSnapShot;
//
//public class Hook extends BaseUtil {
//
//    private BaseUtil base;
//
//    public Hook(BaseUtil base) {
//        this.base = base;
//    }
//
//    @Parameters("mybrowser")
//    @Before("@chrome")
//    public void InitialiseChromeTest() {
//        launchbrowser("chrome");
//        System.out.println("This is Before Class");
//    }
//
//    @Before("@opera")
//    public void InitialiseFirefoxTest() {
//
//        launchbrowser("opera");
//        System.out.println("No data in Before class");
//    }
//
//    @After
//    public void TearDown(Scenario scenario) {
//        System.out.println("Closing browser");
//        if (scenario.isFailed()) {
//            //take screenshot
//            try {
//                takeSnapShot(base.automationDriver, "src\\test\\resources\\snapshots\\fail.png");
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        base.automationDriver.close();
//    }
//
//    private void launchbrowser(String mybrowser) {
//        if (mybrowser.equalsIgnoreCase("firefox")) {
////            WebDriverManager.firefoxdriver().setup();
//            System.out.println("Opening firefox browser");
////            System.setProperty("webdriver.firefox.bin","C:\\Program Files\\Mozilla Firefox\\firefox.exe");
//            System.setProperty("webdriver.gecko.driver", "src\\test\\resources\\drivers\\geckodriver.exe");
//
//            base.automationDriver = new FirefoxDriver();
//        } else if (mybrowser.equalsIgnoreCase("chrome")) {
//            System.out.println("Opening chrome browser");
//            System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\drivers\\chromedriver.exe");
//            base.automationDriver = new ChromeDriver();
////            String username = "mecool2@outlook.com";
////            String authkey = "u0ac70930e699db4";
////
////            DesiredCapabilities caps = new DesiredCapabilities();
//////            caps.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
////            caps.setCapability("name", "Login Form Example");
////            caps.setCapability("build", "1.0");
////            caps.setCapability("browserName", "chrome");
////            caps.setCapability("version", "11");
////            caps.setCapability("platform", "Windows 10");
////            caps.setCapability("screenResolution", "1366x768");
////            caps.setCapability("record_video", "true");
////
////
////            try {
////                base.automationDriver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey +"@hub.crossbrowsertesting.com:80/wd/hub"), caps);
////            } catch (MalformedURLException e) {
////                e.printStackTrace();
////            }
////            System.out.println(base.automationDriver.getSessionId()); ThDes
//        } else if (mybrowser.equalsIgnoreCase("opera")) {
//            System.setProperty("webdriver.opera.driver", "src\\test\\resources\\drivers\\operadriver.exe");
//            OperaOptions options = new OperaOptions();
//            options.setBinary(new File("C:\\Program Files\\Opera\\51.0.2830.55\\opera.exe"));
//            base.automationDriver = new OperaDriver(options);
//        }
//        base.automationDriver.manage().window().maximize();
////        base.automationDriver.get("http://automationpractice.com/index.php");
//        base.automationDriver.get("https://www.visitscotland.com/");
//    }
//
//
//}
//
//
