package com.steps;

import com.appModules.createUser_Action;
import com.helpers.Helpers;
import com.pages.mainPage;
import com.pages.newUserPage;
import com.base.BaseUtil;
import com.webDriver.Driver;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class ScotlandLogin {


    @Given("^I have successfully accessed visit Scotland page$")
    public void iHaveSuccessfullyAccessedVisitScotlandPage() {

        createUser_Action.openwebsite();
        Assert.assertEquals(Driver.webdriver.getTitle(),"VisitScotland - Scotland's National Tourist Organisation");
        System.out.println("Visit Scotland Page opens succesfully");
    }

    @Given("^I register new user$")
    public void iRegisterNewUser() {
//        Page Factory Example
        newUserPage newuser = new newUserPage(Driver.getCurrentDriver());
        newuser.CreateNewAccount();
    }

    @When("^I have validated all headings and they data$")
    public void iHaveValidatedAllHeadingsAndTheyData() {
//        Page Factory Example
//        mainPage main = new mainPage(Driver.getCurrentDriver());
//        main.validateHeader();
//        main.validateDestinations();

    }

    @Then("^new user should be able to login using theie details$")
    public void newUserShouldBeAbleToLoginUsingTheieDetails() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Given("^I have the required login details$")
    public void iHaveTheRequiredLoginDetails() {
        // Write code here that turns the phrase above into concrete actions

    }

    @When("^I login using  provided all required details$")
    public void iLoginUsingProvidedAllRequiredDetails() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("^new user should be able to login using these details$")
    public void newUserShouldBeAbleToLoginUsingTheseDetails() {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}
