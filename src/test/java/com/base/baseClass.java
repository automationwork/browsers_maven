//package com.Base;
//
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.Queue;
//
//import org.openqa.selenium.Platform;
//import org.openqa.selenium.UnexpectedAlertBehaviour;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.remote.BrowserType;
//import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
//import org.xml.sax.XMLReader;
//
//
//public class baseClass {
//
//    public WebDriver driver;
//
//    /* ArrayList for Browsers and IP Ports */
//    static public ArrayList<String> browser 		= new ArrayList<String>();
//    static public ArrayList<String> ipPort 			= new ArrayList<String>();
//
//    /* Queue of Strings to get all the connected devices */
//    static public AndroidUtils androidUtils 		= new AndroidUtils();
//    private static Queue<String> connectedDevices 	= new LinkedList<>(androidUtils.getConnectedDevices());
//
//    // static int CURRENT_BROWSER=-1;
//    int CURRENT_BROWSER 							= -1;
//
//    /* Constants for Browsers */
//    static final int INTERNET_EXPLORER 				= 1;
//    static final int FIREFOX 						= 2;
//    static final int CHROME 						= 3;
//    static final int IOS 							= 4;
//    static final int ANDROID 						= 5;
//    static final int MAC 							= 6;
//    static String SelectedBrowserName 				= "NONE";
//
//    /* configuration.properties instance to get read properties */
//    public EnvironmentPropertiesReader propReader = EnvironmentPropertiesReader.getInstance();
//    public XMLReader xml;
//
//    /***
//     * default constructor initializes config.xml object
//     */
//    public DriverFactory() {
//        xml = new XMLReader("config.xml");
//    }
//
//    /***
//     * function to get Browsers and their IPs from config.xml according to the
//     * NUMOFTHREADS
//     */
//    public void ipBrowserConfig() {
//        for (int counter = 0; counter < Integer.parseInt(xml.readTagVal("NUMOFTHREADS")); counter++) {
//            browser.add(xml.readTagVal("BROWSER" + (counter + 1)));
//            ipPort.add(xml.readTagVal("IPPORT" + (counter + 1)));
//        }
//    }
//
//    /***
//     * function to return WedDriver with capabilities corresponding to the browser
//     * mentioned in config.xml file. Also it controls the execution on CBT or local
//     * environment
//     *
//     * @param name
//     * @return
//     */
//    public WebDriver OpenBrowser(String name) {
//
//        String currentipport 	= ipPort.get(Integer.parseInt(name) - 1);
//        String currentBrowser 	= browser.get((Integer.parseInt(name) - 1));
//
//        System.out.println(currentipport + " on browser " + browser.get((Integer.parseInt(name) - 1)));
//
//        /* condition to check the current browser name and assign the constant to it */
//        if (currentBrowser.equalsIgnoreCase("IE")) {
//            CURRENT_BROWSER = 1;
//        } else if (currentBrowser.equalsIgnoreCase("FF")) {
//            CURRENT_BROWSER = 2;
//        } else if (currentBrowser.equalsIgnoreCase("GC")) {
//            CURRENT_BROWSER = 3;
//        } else if (currentBrowser.equalsIgnoreCase("IOS_SAFARI")) {
//            CURRENT_BROWSER = 4;
//        } else if (currentBrowser.contains("ANDROID")) {
//            CURRENT_BROWSER = 5;
//        } else if (currentBrowser.equalsIgnoreCase("MAC_SAFARI")) {
//            CURRENT_BROWSER = 6;
//        }
//
//        try {
//
//            DesiredCapabilities capabilities = new DesiredCapabilities();
//            capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//            capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
//
//            /* switch block to return the WebDriver according to the browser */
//            switch (CURRENT_BROWSER) {
//
//                case INTERNET_EXPLORER:
//                    try {
//                        SelectedBrowserName 				= "Internet Explorer";
//                        DesiredCapabilities ieCapabilities 	= DesiredCapabilities.internetExplorer();
//
//                        /* condition to handle local or CBT environment */
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            ieCapabilities.setCapability("name", "VTB Automation Internet Explorer");
//                            ieCapabilities.setCapability("browserName", "Internet Explorer");
//                            ieCapabilities.setCapability("version", "11");
//                            ieCapabilities.setCapability("platform", "Windows 8.1");
//                            ieCapabilities.setCapability("screenResolution", "1920x1080");
//                            ieCapabilities.setCapability("record_video", "true");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),ieCapabilities);
//                        } else {
//                            ieCapabilities.setPlatform(Platform.ANY);
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)), ieCapabilities);
//                        }
//
//                        driver.manage().window().maximize();
//
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open Internet Explorer Browser " + e.getMessage());
//                    }
//                    break;
//
//                case FIREFOX:
//                    try {
//                        SelectedBrowserName 					= "Mozilla FireFox";
//                        DesiredCapabilities firefoxCapabilities = DesiredCapabilities.firefox();
//
//                        firefoxCapabilities.setCapability("acceptSslCerts", true);
//                        firefoxCapabilities.setCapability("acceptInsecureCerts", true);
//
//                        /* condition to handle local or CBT environment */
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            firefoxCapabilities.setCapability("name", "VTB Automation Firefox");
//                            firefoxCapabilities.setPlatform(Platform.WIN8);
//                            firefoxCapabilities.setCapability("screenResolution", "1920x1080");
//                            firefoxCapabilities.setCapability("record_video", "true");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),firefoxCapabilities);
//                        } else {
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)), firefoxCapabilities);
//                        }
//
//                        driver.manage().window().maximize();
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open Firefox Browser " + e.getMessage());
//                    }
//                    break;
//                case CHROME:
//                    try {
//                        SelectedBrowserName 					= "Google Chrome";
//                        DesiredCapabilities chromeCapabilities 	= DesiredCapabilities.chrome();
//
//                        /* condition to handle local or CBT environment */
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            chromeCapabilities.setCapability("name", "VTB Automation Chrome");
//                            chromeCapabilities.setPlatform(Platform.WIN8);
//                            chromeCapabilities.setCapability("screenResolution", "1920x1080");
//                            chromeCapabilities.setCapability("record_video", "true");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),chromeCapabilities);
//                        } else {
//                            chromeCapabilities.setPlatform(Platform.ANY);
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)), chromeCapabilities);
//                        }
//
//                        driver.manage().window().maximize();
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open Chrome Browser " + e.getMessage());
//                    }
//                    break;
//
//                case ANDROID:
//                    try {
//                        SelectedBrowserName 							= "android chrome";
//                        DesiredCapabilities androidDesiredCapabilities 	= DesiredCapabilities.android();
//
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            androidDesiredCapabilities.setCapability("browserName", "Chrome");
//                            androidDesiredCapabilities.setCapability("platformName", "Android");
//
//                            if (currentBrowser.equalsIgnoreCase("Android1")) {
//                                androidDesiredCapabilities.setCapability("name", "VTB Automation Galaxy Tab 2");
//                                androidDesiredCapabilities.setCapability("deviceName", "Galaxy Tab 2");
//                                androidDesiredCapabilities.setCapability("platformVersion", "4.1");
//                                androidDesiredCapabilities.setCapability("deviceOrientation", "landscape");
//                            } else if (currentBrowser.equalsIgnoreCase("Android2")) {
//                                androidDesiredCapabilities.setCapability("name", "VTB Automation Galaxy S6");
//                                androidDesiredCapabilities.setCapability("deviceName", "Galaxy S6");
//                                androidDesiredCapabilities.setCapability("platformVersion", "5.0");
//                                androidDesiredCapabilities.setCapability("deviceOrientation", "portrait");
//                            }
//
//                            androidDesiredCapabilities.setCapability("record_video", "true");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),androidDesiredCapabilities);
//                        } else {
//                            androidDesiredCapabilities.setCapability("newCommandTimeout", 60 * 3);
//                            androidDesiredCapabilities.setCapability("noReset", true);
//                            androidDesiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//                            androidDesiredCapabilities.setCapability(CapabilityType.BROWSER_NAME, BrowserType.CHROME);
//                            androidDesiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, connectedDevices.poll());
//                            androidDesiredCapabilities.setCapability("platformName", "android");
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)),androidDesiredCapabilities);
//                        }
//
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open Android Chrome Browser " + e.getMessage());
//                    }
//                    break;
//
//                case IOS:
//                    try {
//                        SelectedBrowserName 						= "safari on iOS";
//                        DesiredCapabilities iOSDesiredCapabilities 	= DesiredCapabilities.iphone();
//
//                        /* condition to handle local or CBT environment */
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            iOSDesiredCapabilities.setCapability("name", "VTB Automation execution on iOS safari browser");
//                            iOSDesiredCapabilities.setCapability("browserName", "Safari");
//                            iOSDesiredCapabilities.setCapability("deviceName", "iPhone 7");
//                            iOSDesiredCapabilities.setCapability("platformVersion", "10.0");
//                            iOSDesiredCapabilities.setCapability("platformName", "iOS");
//                            iOSDesiredCapabilities.setCapability("deviceOrientation", "portrait");
//                            iOSDesiredCapabilities.setCapability("record_video", "true");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),iOSDesiredCapabilities);
//                        } else {
//                            iOSDesiredCapabilities.setCapability("platformName", "iOS");
//                            iOSDesiredCapabilities.setCapability("automationName", "XCUITest");
//                            iOSDesiredCapabilities.setCapability("browserName", "Safari");
//                            iOSDesiredCapabilities.setCapability("newCommandTimeout", 60 * 3);
//                            iOSDesiredCapabilities.setCapability("noReset", true);
//
//                            iOSDesiredCapabilities.setCapability("deviceName", propReader.getProperty("iOSDeviceName"));
//                            iOSDesiredCapabilities.setCapability(MobileCapabilityType.UDID,"6a586e41877c49614381012cf7321bc5e5e8f5a3");
//
//                            iOSDesiredCapabilities.setCapability("bundleId", "com.soprasteria123");
//
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)),iOSDesiredCapabilities);
//                        }
//
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open IOS Safari Browser " + e.getMessage());
//                    }
//                    break;
//
//                case MAC:
//                    try {
//                        SelectedBrowserName 							= "safari on Mac";
//                        DesiredCapabilities safariDesiredCapabilities 	= DesiredCapabilities.safari();
//
//                        /* condition to handle local or CBT environment */
//                        if (propReader.getProperty("isCBT").equalsIgnoreCase("true")) {
//                            safariDesiredCapabilities.setCapability("name", "VTB automationon MAc safari browser");
//                            safariDesiredCapabilities.setCapability("browserName", "Safari");
//                            safariDesiredCapabilities.setCapability("version", "10");
//                            safariDesiredCapabilities.setCapability("platform", "Mac OSX 10.12");
//                            safariDesiredCapabilities.setCapability("screenResolution", "1366x768");
//                            safariDesiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//                            safariDesiredCapabilities.setCapability("name", "VTB Automation execution on MAC safari browser");
//
//                            driver = new RemoteWebDriver(new URL(propReader.getProperty("CBTUrl")),safariDesiredCapabilities);
//                            break;
//                        } else {
//                            safariDesiredCapabilities.setBrowserName("safari");
//                            safariDesiredCapabilities.setPlatform(Platform.MAC);
//                            safariDesiredCapabilities.setVersion("10.12.6");
//                            safariDesiredCapabilities.setAcceptInsecureCerts(true);
//                            safariDesiredCapabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
//                            driver = new RemoteWebDriver(new URL(ipPort.get(Integer.parseInt(name) - 1)),safariDesiredCapabilities);
//                            driver.manage().window().maximize();
//                        }
//
//                    } catch (Exception e) {
//                        System.out.println("Error ID 46: Could not open MAC Safari Browser " + e.getMessage());
//                    }
//                    break;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("Error ID 46: Could not open Browser " + e);
//            System.out.println("Error ID 46: Could not open Browser ");
//        }
//        return driver;
//    }
//
//}
