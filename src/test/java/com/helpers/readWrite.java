package com.helpers;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class readWrite {

    public void writing(String value,String filePath) {
        try {
            FileWriter fw = new FileWriter(filePath, true); //the true will append the new data
            fw.write(value + "\n");//appends the string to the file
            fw.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());
        }
    }

    public String[] reading(String filePath){
        List<String> list = new ArrayList<String>();
        try{
            Scanner s = new Scanner(new File(filePath));
            while (s.hasNext()){
                list.add(s.nextLine());
            }
            s.close();
        } catch (IOException ioe) {
            System.err.println("IOException: " + ioe.getMessage());

        }
            return list.toArray(new String[0]);
    }
}
