
package com.helpers;

import org.openqa.selenium.*;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class Helpers {

    public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {

        TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
        File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
        File DestFile = new File(fileWithPath);
        FileUtils.copyFile(SrcFile, DestFile);
    }

    public void browsername(String browser1) throws IOException {

        File configFile = new File("src/test/resources/config.properties");

        FileInputStream in = new FileInputStream(configFile);
        Properties props = new Properties();
        props.load(in);
        in.close();

        FileOutputStream out = new FileOutputStream(configFile);
        
        props.setProperty("seleniumEnvironment","local");
        props.setProperty("browser", browser1);
        props.setProperty("browserVersion","");
        props.setProperty("platform","");
        props.setProperty("platformVersion","");
        props.setProperty("seleniumHub","");
        props.setProperty("browserstackResolution","");
        props.setProperty("browserstackUsername","");
        props.setProperty("browserstackPasswrd","");
        props.setProperty("browserstackProject","");
        props.setProperty("browserstackBuild","");

        props.store(out, null);
        out.close();
    }
}


