package com.cucumberTest;

import cucumber.api.CucumberOptions;
import cucumber.api.Scenario;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import java.util.Map;
import com.cucumber.listener.Reporter;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import java.io.File;
import java.util.Properties;


@CucumberOptions(
//        features = {"src/test/resources/scotlandLogin.feature", "src/test/resources/scotlandLogin2.feature",
//                "src/test/resources/login.feature"},
        features = {"src/test/resources/login.feature"},
        tags = {"@smoke, @regression, @functional"},
        glue = {"com.steps"},
        monochrome = true,
        plugin = {"com.cucumber.listener.ExtentCucumberFormatter:reports/report.html"},
        format = {"pretty",
                "html:target/site/cucumber-pretty",
                "rerun:target/rerun.txt",
                "json:target/cucumber1.json"})


public class TestRunner extends AbstractTestNGCucumberTests {

    public static Map<String, String> config;
    public static Scenario scenario;

    @AfterClass
    public static void createReport() {

//        Reporter.loadXMLConfig((new File(System.getProperty("C:\\Users\\hsidhu\\Documents\\AutomationProjects\\Demo\\browsers\\src\\test\\resources\\extent-config.xml"))));
        Reporter.loadXMLConfig("C:\\Users\\hsidhu\\Documents\\AutomationProjects\\Demo\\browsers\\src\\test\\resources\\extent-config.xml");
        Reporter.setSystemInfo("OS", System.getProperty("os.name"));
        Reporter.setTestRunnerOutput("Sample test runner output message");
        Reporter.setSystemInfo("Project Name", "Sopra Steria demo Project");
        Reporter.setSystemInfo("Environment", "QA - demo");
        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
        Reporter.setSystemInfo("User Time zone", System.getProperty("user.timezone"));

//        Properties p = System.getProperties();
//        p.list(System.out);
    }

//    @AfterClass
//    public static void reportSetup() {
////        Reporter.loadXMLConfig(new File("C:\\Users\\hsidhu\\Documents\\AutomationProjects\\Demo\\browsers\\src\\test\\resources\\extent-config.xml"));
//        Properties p = System.getProperties();
//        p.list(System.out);
//
////        Reporter.setSystemInfo("User Name", System.getProperty("user.name"));
////        Reporter.setSystemInfo("Time Zone", System.getProperty("user.timezone"));
////        Reporter.setSystemInfo("64 Bit", "Windows 10");
////        Reporter.setSystemInfo("2.53.0", "Selenium");
////        Reporter.setSystemInfo("3.3.9", "Maven");
////        Reporter.setSystemInfo("1.8.0_66", "Java Version");
////        Reporter.setTestRunnerOutput("Cucumber Test Runner");
//    }

//    @Test
//    @Parameters("browser")
//    public String hello(String st){return st;};

}
