package com.cucumberTest;

import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.TestNGCucumberRunner;
import org.testng.annotations.*;


// THIS EXTENDED CUCUMBER CLASS WILL HELP RESRUN THE FAILED TEST CASES FOR SAID NUMBER OF TIMES
// Maven DEPENDENCY : https://mvnrepository.com/artifact/com.github.mkolisnyk/cucumber-runner/1.0.9

@ExtendedCucumberOptions(
        jsonReports = "target/cucumber.json",
        retryCount = 3,
        detailedReport = true,
        detailedAggregatedReport = true,
        overviewReport = true,
        coverageReport = true,
        jsonUsageReport = "target/cucumber-usage.json",
        usageReport = true,
        toPDF =true,
//        includeCoverageTags = {"@chrome"},
        excludeCoverageTags = {},
        outputFolder = "target"
)

@CucumberOptions(
        features = "src/test/resources/scotlandLogin.feature",
        tags = "@chrome",
        glue = {"com.steps"},
        monochrome = true,
        format = {"pretty",
                "html:target/site/cucumber-pretty",
                "rerun:target/rerun.txt",
                "json:target/cucumber1.json"}
)

public class RegressionChrome {

    @Test(groups = "examples-testng", description = "Example of using TestNGCucumberRunner to invoke Cucumber")
    public void runCukes() {
        new TestNGCucumberRunner(getClass()).runCukes();
    }
}



