package com.cucumberTest;


//import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
//import cucumber.api.junit.Cucumber;

//@RunWith(Cucumber.class)
@CucumberOptions(
                features = {"src/test/resources"},
                tags = "@opera",
                glue = {"com.steps"},
                format = { "pretty",
                        "html:target/site/cucumber-pretty",
                        "rerun:target/rerun.txt",
                        "json:target/cucumber1.json" }
)
public class RegressionOpera extends AbstractTestNGCucumberTests{

}