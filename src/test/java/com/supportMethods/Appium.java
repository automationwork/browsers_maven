//package com.supportMethods;
//
//import com.cucumberTest.TestRunner;
//import io.appium.java_client.remote.MobileCapabilityType;
//import org.openqa.selenium.WebDriverException;
//import org.openqa.selenium.remote.BrowserType;
//import org.openqa.selenium.remote.DesiredCapabilities;
//
//
//public class Appium {
//    public static Boolean useAppium() {
//
//        return Boolean.valueOf(TestRunner.config.get("useBrowserstack"));
//    }
//
//    public static void setSeleniumHub() {
//
//        String browserstackUsername = TestRunner.config.get("browserstackUsername");
//
//        if (browserstackUsername == null) {
//            throw new WebDriverException("Browserstack username not set.");
//        }
//
//        String browserstackPassword = TestRunner.config.get("browserstackPassword");
//
//        if (browserstackPassword == null) {
//            throw new WebDriverException("Browserstack password not set.");
//        }
//
//        TestRunner.config.put("seleniumHub", "http://" + browserstackUsername + ":" + browserstackPassword + "@hub-cloud.browserstack.com/wd/hub");
//    }
//
//    public static DesiredCapabilities setBrowserCapabilities() {
//        String browser_name = TestRunner.config.get("browser");
//        String platform_name = TestRunner.config.get("platform");
//        DesiredCapabilities android = DesiredCapabilities.android();
//
//        android.setCapability(MobileCapabilityType.PLATFORM_NAME, platform_name);
//
//        if (browser_name.equalsIgnoreCase("chrome")) {
//            android.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);
//        } else if (browser_name.equalsIgnoreCase("ie")) {
//            android.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.IE);
//        } else if (browser_name.equalsIgnoreCase("firefox")) {
//            android.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.FIREFOX);
//        }
//
//        return android;
//    }
//}
