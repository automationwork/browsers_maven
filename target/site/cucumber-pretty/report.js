$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/login.feature");
formatter.feature({
  "line": 1,
  "name": "Demo project Feature:",
  "description": "",
  "id": "demo-project-feature:",
  "keyword": "Feature"
});
formatter.before({
  "duration": 69895881,
  "status": "passed"
});
formatter.before({
  "duration": 87795,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Validate email creation process",
  "description": "",
  "id": "demo-project-feature:;validate-email-creation-process",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 2,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 4,
  "name": "I am on the home page and select the sign in button",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I provide a dummy email",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "I Select Dresses and casual dresses",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "only one product is made available",
  "keyword": "Then "
});
formatter.match({
  "location": "RetailShopping.iAmOnTheHomePageAndSelectTheSignInButton()"
});
formatter.result({
  "duration": 54625812052,
  "status": "passed"
});
formatter.match({
  "location": "RetailShopping.iProvideADummyEmail()"
});
formatter.result({
  "duration": 653837933,
  "status": "passed"
});
formatter.match({
  "location": "RetailShopping.iSelectDressesAndCasualDresses()"
});
formatter.result({
  "duration": 48820,
  "status": "passed"
});
formatter.match({
  "location": "RetailShopping.onlyOneProductIsMadeAvailable()"
});
formatter.result({
  "duration": 49231,
  "status": "passed"
});
formatter.after({
  "duration": 117744,
  "status": "passed"
});
formatter.before({
  "duration": 2992414,
  "status": "passed"
});
formatter.before({
  "duration": 39385,
  "status": "passed"
});
formatter.scenario({
  "line": 10,
  "name": "Validate links and other options on Scotland Page",
  "description": "",
  "id": "demo-project-feature:;validate-links-and-other-options-on-scotland-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 9,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 11,
  "name": "I have successfully accessed visit Scotland page",
  "keyword": "Given "
});
formatter.step({
  "line": 12,
  "name": "I register new user",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I have validated all headings and they data",
  "keyword": "When "
});
formatter.step({
  "line": 14,
  "name": "new user should be able to login using these details",
  "keyword": "Then "
});
formatter.match({
  "location": "ScotlandLogin.iHaveSuccessfullyAccessedVisitScotlandPage()"
});
formatter.result({
  "duration": 4877067335,
  "error_message": "org.openqa.selenium.NoSuchWindowException: no such window: target window already closed\nfrom unknown error: web view not found\n  (Session info: chrome\u003d66.0.3359.117)\n  (Driver info: chromedriver\u003d2.36.540470 (e522d04694c7ebea4ba8821272dbef4f9b818c91),platform\u003dWindows NT 10.0.15063 x86_64) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 0 milliseconds\nBuild info: version: \u00273.9.1\u0027, revision: \u002763f7b50\u0027, time: \u00272018-02-07T22:25:02.294Z\u0027\nSystem info: host: \u0027ITEM-S33151\u0027, ip: \u002710.46.30.70\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00279.0.4\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, acceptSslCerts: false, applicationCacheEnabled: false, browserConnectionEnabled: false, browserName: chrome, chrome: {chromedriverVersion: 2.36.540470 (e522d04694c7eb..., userDataDir: C:\\Users\\hsidhu\\AppData\\Loc...}, cssSelectorsEnabled: true, databaseEnabled: false, handlesAlerts: true, hasTouchScreen: false, javascriptEnabled: true, locationContextEnabled: true, mobileEmulationEnabled: false, nativeEvents: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: XP, platformName: XP, rotatable: false, setWindowRect: true, takesHeapSnapshot: true, takesScreenshot: true, unexpectedAlertBehaviour: , unhandledPromptBehavior: , version: 66.0.3359.117, webStorageEnabled: true}\nSession ID: f77508f18a3f697a12d5e3a3fe404326\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat java.base/jdk.internal.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.base/java.lang.reflect.Constructor.newInstance(Constructor.java:488)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:214)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:166)\r\n\tat org.openqa.selenium.remote.http.JsonHttpResponseCodec.reconstructValue(JsonHttpResponseCodec.java:40)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:80)\r\n\tat org.openqa.selenium.remote.http.AbstractHttpResponseCodec.decode(AbstractHttpResponseCodec.java:44)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:160)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:658)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.getTitle(RemoteWebDriver.java:329)\r\n\tat com.steps.ScotlandLogin.iHaveSuccessfullyAccessedVisitScotlandPage(ScotlandLogin.java:22)\r\n\tat ✽.Given I have successfully accessed visit Scotland page(src/test/resources/login.feature:11)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "ScotlandLogin.iRegisterNewUser()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ScotlandLogin.iHaveValidatedAllHeadingsAndTheyData()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "ScotlandLogin.newUserShouldBeAbleToLoginUsingTheseDetails()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 60204382,
  "status": "passed"
});
});