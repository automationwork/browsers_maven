Feature: Demo project Feature:
  @smoke
  Scenario: Validate email creation process
    Given  I am on the home page and select the sign in button
    When I provide a dummy email
    And I Select Dresses and casual dresses
    Then only one product is made available

  @smoke
  Scenario: Validate links and other options on Scotland Page
    Given  I have successfully accessed visit Scotland page
    And I register new user
    When I have validated all headings and they data
    Then new user should be able to login using these details
